const { withStyles, Box, Container, Grid, AppBar, BottomNavigation, Toolbar, Typography } = MaterialUI;

var styles = theme => ({ colorPrimary : { backgroundColor : '#673337' } });

class Index extends React.Component
{
    render()
    {
        const { classes } = this.props;

        return (
        <React.Fragment>
        <AppBar position="static" color="primary" className={classes.colorPrimary} style={{ borderRadius : 3 }}>
            <Toolbar variant="dense"><Typography>Coffe Shop</Typography></Toolbar>
        </AppBar>
        <Box component="main">
            <Container maxWidth="xl">
                <Grid container>
                    <Grid item xs={3}></Grid><Grid item xs={3}>!</Grid><Grid item xs={3}>!</Grid><Grid item xs={3}>!</Grid>
                </Grid>
                <Grid container>
                    <Grid item xs={3}></Grid><Grid item xs={3}>!</Grid><Grid item xs={3}>!</Grid><Grid item xs={3}>!</Grid>
                </Grid>
                <Grid container>
                    <Grid item xs={3}></Grid><Grid item xs={3}>!</Grid><Grid item xs={3}>!</Grid><Grid item xs={3}>!</Grid>
                </Grid>
            </Container>
        </Box>
        <BottomNavigation></BottomNavigation>
        </React.Fragment>
        );
    }
}

export default withStyles(styles)(Index);